# dk-elastic
To set Up elastic Index  
PUT http://127.0.0.1:9200/product/  
```json
{
  "mappings": {
    "_doc": {
      "properties": {
        "variant": {
          "type": "nested",
          "properties": {
            "color":  { "type": "keyword" },
            "price": { "type": "integer"  }
          }
        },
        "title": {
          "type":  "text"
        },
        "description": {
          "type":  "text"
        },
        "date": {
          "type":   "date",
          "format": "epoch_millis"
        }
      }
    }
  }
}
```
___________
Create MySql DB And Migrate to create User table
___________

Links  
/login  
/register  
/products/search  
/products/new
