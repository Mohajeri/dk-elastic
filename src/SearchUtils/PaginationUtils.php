<?php

/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 12:34 AM
 */
namespace App\SearchUtils;

class PaginationUtils
{
    private static $defaultFrom = 0;
    private static $minSize = 5;
    private static $maxSize = 20;

    public static function createFrom($from) :int
    {
        $f = (int)$from;
        return ($f > 0 ? $f : 0);
    }

    public static function createPage($page) :int
    {
        $p = (int)$page;
        return ($p > 0 ? $p : 0);
    }


    public static function createSize($size) :int
    {
        $s = (int)$size;
        if ($s >= self::$minSize & $s <= self::$maxSize) {
            return $s;
        } elseif ($s > self::$maxSize) {
            return self::$maxSize;
        } else {
            return self::$minSize;
        }
    }
}