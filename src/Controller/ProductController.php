<?php
/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/27/18
 * Time: 8:49 AM
 */

namespace App\Controller;

use App\Elastic\ElasticSearchApi;
use App\Entity\Product;
use App\Redis\RedisCache;
use App\SearchUtils\PaginationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{

    /**
     * @Route("/products/search",name="products_search")
     */
    public function search(Request $request)
    {
        $q = urldecode($request->get('q'));
        $queryColors = $request->query->get('colors');
        $price_limit = $request->query->get('price_limit');

        $size= PaginationUtils::createSize($request->query->get('size'));
        $page= PaginationUtils::createFrom($request->query->get('page'));
        $from = $page * $size;
        $esp = new ElasticSearchApi();


        $response = $esp->searchProducts($q);

        $filteredResponse = $esp->searchAndFilter($q,$size,$from,$queryColors,$price_limit);

        RedisCache::pushItemsToCache($filteredResponse['hits']['hits']);

        $colors = $esp->extractColorAggs($response);
        $max_price = $esp->extractMaxPrice($response);
        $min_price = $esp->extractMinPrice($response);
        $total = $esp->extractTotal($filteredResponse);
        $total_pages = ceil($total / $size);

        return $this->render('product/all.html.twig', array(
            'prods' => $filteredResponse['hits']['hits'],
            'colors' => $colors,
            'max_price' => $max_price,
            'size'=>$size,
            'from'=>$from,
            'total'=>$total,
            'min_price' => $min_price,
            'total_pages' => $total_pages,
            'queryColors' => explode(',',$queryColors),
            'page' => $page
        ));
    }

    /**
     * @Route("/products/show/{id}",name="products_show")
     */
    public function show($id) {

        if(RedisCache::getClient()->exists($id)) {
            $p = RedisCache::getItemFromCache($id);
        } else {
            $p = Product::getFromElastic($id);
        }

        return $this->render('product/show.html.twig', array(
            'product' => $p
        ));
    }

}