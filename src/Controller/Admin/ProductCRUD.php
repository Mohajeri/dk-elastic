<?php

/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 9:15 AM
 */

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\Variant;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProductCRUD extends Controller
{
    /**
     * @Route("/products/new",name="products_new")
     */
    public function new(Request $request)
    {
        $p = new Product();

        $form = $this->createForm(ProductType::class,$p);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... maybe do some form processing, like saving the Task and Tag objects

            $p->persistToElastic();

            return $this->redirectToRoute('products_search');
        }

        return $this->render('product/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/products/update/{id}", name="products_update")
     */

    public function update($id,Request $request)
    {

        $p = Product::getFromElastic($id);
        $p->setId($id);
        $form = $this->createForm(ProductType::class,$p);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // ... maybe do some form processing, like saving the Task and Tag objects

            $p->persistToElastic();

            return $this->redirectToRoute('products_search');
        }

        return $this->render('product/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}