<?php

/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 11:52 PM
 */

namespace App\Redis;

use App\Entity\Product;
use App\Entity\Variant;
use Predis\Client;
use Predis\Configuration\Options;

class RedisCache
{
    public static function getClient()
    {
        $client = new Client();
        return $client;
    }

    public static function pushItemsToCache($items)
    {
        foreach ($items as $product) {
            self::getClient()->set($product['_id'], serialize($product));
        }
    }




    public static function getItemFromCache($id)
    {
        $r = self::getClient()->get($id);
        $response = unserialize($r,array(\stdClass::class));
        $p = new Product();
        $p->setTitle($response['_source']['title']);
        $p->setDescription($response['_source']['description']);
        $p->setId($response['_id']);
        if(isset($response['_source']['variant']) && \count($response['_source']['variant'])>0) {
            foreach ($response['_source']['variant'] as $v) {
                $variant = new Variant();
                $variant->setColor($v['color']);
                $variant->setPrice($v['price']);
                $p->addVariant($variant);
            }
        }
        return $p;
    }
}