<?php


/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/29/18
 * Time: 11:30 AM
 */

namespace App\Elastic;

use Elasticsearch\ClientBuilder;

class ElasticConnection
{
    private static $hosts = [
        '127.0.0.1:9200'
    ];

    public static function getElasticConnection() {
        $client = ClientBuilder::create()
        ->setHosts(self::$hosts)
        ->build();
        return $client;
    }
}