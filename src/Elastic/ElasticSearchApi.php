<?php
/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/29/18
 * Time: 11:44 AM
 */

namespace App\Elastic;


class ElasticSearchApi
{
    private $index = 'product';

    public function searchAndFilter($search_phrase = '', $size = 10, $from = 0, $colors = '', $price = 100000000) : array
    {
        $client = ElasticConnection::getElasticConnection();

        $query = array();
        if ($search_phrase !== '') {
            $query['bool']['must'] = [

                'bool' => [
                    'should' => [
                        ['match' => ['title' => $search_phrase]],
                        ['match' => ['description' => $search_phrase]]
                    ]
                ]
            ];
        }

        if ($colors != '') {
            $query['bool']['filter'] = [
                'nested' => [
                    'path' => 'variant',
                    'query' => [
                        'bool' => [
                            'must' => [
                                ['terms' => ['variant.color' => explode(',', $colors)]],
                                ['range' => ['variant.price' => ['lte' => $price, 'gte' => 0]]],
                            ]
                        ]
                    ]
                ]
            ];
        }
        else {
            $query['bool']['filter'] = [
                'nested' => [
                    'path' => 'variant',
                    'query' => [
                        'bool' => [
                            'must' => [
                                ['range' => ['variant.price' => ['lte' => $price, 'gte' => 0]]],
                            ]
                        ]
                    ]
                ]
            ];
        }


        $params = [
            'index' => $this->index,
            'body' => [
                'query' => $query,
                'size' => $size,
                'from' => $from,
                'aggs' => [
                    'variant' => [
                        'nested' => ['path' => 'variant'],
                        'aggs' => [
                            'variant_color' => ['terms' => ['field' => 'variant.color']],
                            'variant_max_price' => ['max' => ['field' => 'variant.price']],
                            'variant_min_price' => ['min' => ['field' => 'variant.price']]
                        ]
                    ]
                ]
            ]
        ];


        $response = $client->search($params);

        if (isset($response['hits']['hits']) && \count($response['hits']['hits']) > 0) {
            return $response;
        }
        $emptyResult = array();
        $emptyResult['hits'] = array();
        $emptyResult['hits']['hits'] = array();
        return $emptyResult;
    }

    public function searchProducts($search_phrase = '') : array
    {
        $client = ElasticConnection::getElasticConnection();

        $query = array();
        if ($search_phrase !== '') {
            $query['bool'] = [
                'must' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['title' => $search_phrase]],
                            ['match' => ['description' => $search_phrase]]
                        ]
                    ]
                ]
            ];
        } else {
            $query['match_all'] = new \stdClass();
        }

        $params = [
            'index' => $this->index,
            'body' => [
                'query' => $query,
                'aggs' => [
                    'variant' => [
                        'nested' => ['path' => 'variant'],
                        'aggs' => [
                            'variant_color' => ['terms' => ['field' => 'variant.color']],
                            'variant_max_price' => ['max' => ['field' => 'variant.price']],
                            'variant_min_price' => ['min' => ['field' => 'variant.price']]
                        ]
                    ]
                ]
            ]
        ];


        $response = $client->search($params);

        if (isset($response['hits']['hits']) && \count($response['hits']['hits']) > 0) {
            return $response;
        }

        $emptyResult = array();
        $emptyResult['hits'] = array();
        $emptyResult['hits']['hits'] = array();
        return $emptyResult;
    }


    public function extractColorAggs($response)
    {
        if (
            isset($response['aggregations']['variant']['variant_color']['buckets']) &&
            \count($response['aggregations']['variant']['variant_color']['buckets']) > 0
        ) {
            return $response['aggregations']['variant']['variant_color']['buckets'];
        }
        return array();
    }


    public function extractMaxPrice($response)
    {
        if (
        isset($response['aggregations']['variant']['variant_max_price']['value'])
        ) {
            return $response['aggregations']['variant']['variant_max_price']['value'];
        }
        return 10000000;
    }

    public function extractMinPrice($response)
    {
        if (
        isset($response['aggregations']['variant']['variant_min_price']['value'])
        ) {
            return $response['aggregations']['variant']['variant_min_price']['value'];
        }
        return 0;
    }

    public function extractTotal($response)
    {
        if (
        isset($response['hits']['total'])
        ) {
            return $response['hits']['total'];
        }
        return 0;
    }
}