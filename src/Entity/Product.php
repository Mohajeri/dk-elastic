<?php
/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 1:31 AM
 */

namespace App\Entity;


use App\Elastic\ElasticConnection;
use App\Redis\RedisCache;

class Product
{
    /** @var string */
    private $id = null;

    /** @var string */
    private $title;

    /** @var  string */
    private $description;

    /** @var  Variant[] */
    private $variants;

    public function __construct()
    {
        $this->variants = array();
    }

    /**
     * @param string $color
     * @param int $price
     */
    public function createAndAddVariant(string $color,int $price) {
        array_push($this->variants,new Variant($color,$price));
    }

    /**
     * @param Variant $v
     */
    public function addVariant(Variant $v) {
        array_push($this->variants,$v);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Variant[]
     */
    public function getVariants(): array
    {
        return $this->variants;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @param Variant[] $variants
     */
    public function setVariants(array $variants)
    {
        $this->variants = $variants;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function persistToElastic() {

        $body = array();

        $body['title'] = $this->getTitle();
        $body['description'] = $this->getDescription();
        $body['variant'] = array();
        foreach($this->getVariants() as $v) {
            $vt = array('price'=>$v->getPrice(),'color'=>$v->getColor());
            array_push($body['variant'],$vt);
        }

        echo json_encode($body);

        $client = ElasticConnection::getElasticConnection();

        if($this->getId()!==null)
        {
            $params = [
                'index' => 'product',
                'type' => '_doc',
                'body' => [
                    'doc' => $body
                    ],
                'id' => $this->getId()
            ];

            $response = $client->update($params);
        }
        else {
            $params = [
                'index' => 'product',
                'type' => '_doc',
                'body' => $body
            ];

            $response = $client->index($params);

        }

        return $response;
    }

    public static function getFromElastic($id) {
        $params = [
            'index' => 'product',
            'type' => '_doc',
            'id' => $id
        ];

        $client = ElasticConnection::getElasticConnection();

        $response = $client->get($params);

        $p = new Product();
        if($response['found']) {
            $p = self::createFromElasticGet($response);
            RedisCache::pushItemsToCache(array($response));
        }

        return $p;
    }

    public static function createFromElasticGet($response) {
        $p = new Product();
        $p->setTitle($response['_source']['title']);
        $p->setDescription($response['_source']['description']);
        $p->setId($response['_id']);
        if(isset($response['_source']['variant']) && \count($response['_source']['variant'])>0) {
            foreach ($response['_source']['variant'] as $v) {
                $variant = new Variant();
                $variant->setColor($v['color']);
                $variant->setPrice($v['price']);
                $p->addVariant($variant);
            }
        }
        return $p;
    }
}