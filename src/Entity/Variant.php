<?php
/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 8:46 AM
 */

namespace App\Entity;


class Variant
{

    /** @var string */
    private $color;

    /** @var integer */
    private $price;

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color)
    {
        $this->color = $color;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    public function __construct()
    {

    }
}