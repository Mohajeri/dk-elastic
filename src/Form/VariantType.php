<?php
/**
 * Created by IntelliJ IDEA.
 * User: mostafa
 * Date: 7/30/18
 * Time: 9:08 AM
 */

namespace App\Form;

use App\Entity\Variant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class VariantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price',NumberType::class);

        $builder->add('color', ChoiceType::class, array(
            'choices'  => array(
                'Red' => 'Red',
                'Green' => 'Green',
                'Blue' => 'Blue',
                'Black' => 'Black'
            ),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Variant::class,
        ));
    }
}